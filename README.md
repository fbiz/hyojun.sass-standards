# Hyojun - Padrões de desenvolvimento SASS
`v0.1.0`

Este arquivo contém a documentação do projeto. Cada diretório pode ser considerado um "escopo" e quando necessário, conterá um arquivo README.md para descrever informações relevantes sobre o assunto.

O repositório descreve a organização dos arquivos SASS nos projetos que utilizam o framework [Hyojun][]. O foco é mostrar o que acontece dentro de `~/**/sass/`, a estrutura anterior a este diretório está disponível no repositório do [Hyojun][] [TODO: colocar link para documentação de estrutura de diretórios do mp].

Apresentação sobre a organização dos diretórios e conceitos relacionados aos módulos, layouts, cores e unidades: [Parte 1][presentation-1], [Parte 2][presentation-2].

## Organização dos diretórios

Este diretório contém a seguinte estrutura:

* **~/** - você está aqui;
    * **sass/** - todos os módulos, configurações, mixins, etc. em SASS. [Mais detalhes][sass];
    * **css/** - local onde os arquivos CSS serão gerados — apenas para exemplo;

[↩][top]

## Changelog

Histórico das versões acessível em [CHANGELOG.md][].

[top]: #markdown-header-hyojun-padroes-de-desenvolvimento
[Hyojun]: https://bitbucket.org/fbiz/hyojun/wiki/Home
[sass]: HEAD/sass/
[css]: HEAD/css/
[presentation-1]: https://speakerdeck.com/mcarneiro/2
[presentation-2]: https://speakerdeck.com/mcarneiro/2-1
[CHANGELOG.md]: https://bitbucket.org/fbiz/hyojun.sass-standards/src/HEAD/CHANGELOG.md