# SASS

Todos os arquivos SASS específicos do projeto ficam neste diretório. Existem algumas organizações e práticas que devem ser utilizadas. Veja na lista de diretórios abaixo.

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este diretório contém a seguinte estrutura:

* **~/sass/** - você está aqui;
    * **output/** - arquivos que serão gerados na pasta `~/css/`; [Mais detalhes][output];
    * **source/** - funções, mixins, configurações, módulos e lay-outs do projeto; [Mais detalhes][source];

[↩][top]

## Fazendo o watch do SASS

O formato do comando do sass seria [^1]:

    sass --watch sass/{source,output}:css --load-path=bower_components/

Um build system no projeto do [Sublime Text][] ficaria algo mais ou menos assim:

    #!json
    {
        "build_systems":
        [
            {
                "cmd":
                [
                    "sass",
                    "--load-path=${project_path}/bower_components/",
                    "--stop-on-error",
                    "--trace",
                    "--update",
                    "${project_path}/sass/output:${project_path}/css"
                ],
                "name": "SASS update",
                "path": "/usr/local/bin:/usr/bin/",
                "selector": "source.sass, source.scss"
            }
        ]
    }

## Padrões do Hyojun

Todos os arquivos devem ser do tipo **SCSS**.

Libs externas são sempre baixadas utilizando o [bower][], por isso o parâmetro `--load-path=_bower_components/` na linha de comando acima.

As libs consideradas nos exemplos são: [GS][] e [Bourbon][];

Os arquivos de `source/` sempre terão o prefixo `_` (ex.: "_layout.scss") para serem ignorados na saída do SASS enquanto os arquivos de `output` sempre serão sem prefixo (ex.: "structure.scss").

[↩][top]

[^1]: É considerado que a linha de comando está sendo executada a partir de `~/`, com os diretórios `~/sass`, `~/css` e `~/bower_components` logo abaixo.

[top]: #markdown-header-sass
[output]: output/ 
[source]: source/

[bower]: http://bower.io/
[GS]: https://github.com/mcarneiro/gs
[Bourbon]: http://bourbon.io/
[Sublime Text]: http://www.sublimetext.com/2