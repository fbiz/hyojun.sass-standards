# Source

Todo o código-fonte específico do projeto. A estrutura atual mostra uma situação com diversos "breakpoints" para tamanhos de telas distintos, veja como funciona em "[Tamanhos de tela][]".

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/** - você está aqui;
    * **320-mobile/** - código específico para "mobile". Mais detalhes em "[desktop][desktop]";
    * **768-tablet/** - código específico para "tablet". Mais detalhes em "[desktop][desktop]";
    * **960-desktop/** - código específico para "desktop". [Mais detalhes][desktop];
    * **common/** - código-fonte comum entre todos os tamanhos de tela. [Mais detalhes][common];

Esta estrutura prevê a criação de lay-outs responsivos. É recomendável que esta estrutura seja utilizada mesmo quando o projeto não for produzido para diferentes tamanhos de telas.

**IMPORTANTE**    
A estrutura de pastas internas de cada uma das versões específicas (ex.: mobile, tablet, desktop) devem seguir exatamente a do `common/` mesmo que algum arquivo contenha poucas linhas. Arquivos e diretórios vazios são ignorados.

Ou seja, se tenho a pasta **common/modules/navigation/\_main.scss** e tenho especificidades na versão `320-mobile` terei a mesma estrutura, mesmo que exista apenas 1 arquivo no diretório: **320-mobile/modules/navigation/\_main.scss**.

[↩][top]

## Tamanhos de tela

A organização para layouts responsivos (ou mesmo versões distintas para diferentes tamanhos de telas) consiste em 2 regras simples:

* A pasta `common` contém tudo que for comum entre os diferentes layouts, ex.: cores, fontes, mixins, funções, estrutura de certos módulos, etc;
* Para cada tamanho de tela, existe um diretório seguindo o padrão `[tamanho]-[label]`, caso exista a necessidade, mais diretórios podem ser criados;

**IMPORTANTE**    
o `[tamanho]` utilizado no nome do diretório tem o objetivo de identificação rápida e não, necessariamente, de definição de breakpoint. No caso padrão, `320-mobile` é referente ao layout fluido mobile, que vai até 767px de largura, a mesma coisa para o `768-tablet`, que diz respeito do 768px até 959px, e assim por diante.

[↩][top]

[top]: #markdown-header-source
[Tamanhos de tela]: #markdown-header-tamanhos-de-tela
[common]: common/
[mobile]: mobile/
[tablet]: tablet/
[desktop]: desktop/