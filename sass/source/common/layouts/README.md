# Layouts

Todas as diagramações das páginas se encontram neste diretório. Basicamente existe 1 arquivo para cada página contendo apenas as regras específicas de posicionamento, escopo, etc. Nenhuma regra de módulo é definida nesta página, apenas regras referentes à diagramação.

## Como criar layouts

"Layouts" funcionam como templates, páginas ou diagramações dos módulos. Quando um módulo é criado, ele deve ser fluído e, basicamente, se "encaixar" em qualquer espaço — claro que existem exceções, mas a regra geralmente é essa.

O layout contém as regras destes espaços onde os módulos se encaixarão. Este é o local para definir regras de posicionamento ou escopo. Ex.: preciso colocar um `float`? o elemento precisa ter 50% de largura ou largura fixa de X colunas? Um elemento após o outro tem um espaçamento maior; ou ficam "grudados"; e assim por diante.

Normalmente cada "tipo" de página contém um layout, que em alguns casos pode ser um arquivo por área do site (projetos com mais cara de campanha) ou serem mais parecidos com template (como blogs ou portais).

Alguns exemplos do que seria módulo e layout:

![Layout de exemplo](http://img-fotki.yandex.ru/get/5008/221798411.0/0_babce_7deef28f_XXL.png)

_\*Imagem retirada do site [http://bem.info/]()_

No caso acima **menu block**, **auth block**, **logo block** e **search block** são módulos. Suas versões _standalone_ são fluídas e consideram margens padrões.

O conjunto **head block** consiste no layout:

* _auth block_ posicionado à direita com uma largura fixa de X colunas;
* _logo block_ à esquerda;
* _search block_ ao centro com largura fluída do espaço entre "logo" e "auth" block;
* _menu block_ alinhado ao _search block_ e
* por fim, o holder do header, com um padding, float wrapper, background, etc.

O arquivo do layout é importado nos arquivos do output que os necessitam. Normalmente temos 1 ou 2 layouts por output, sendo o primeiro estrutural e o segundo específico. [Veja como funcionam as regras de import][wrappers].

[↩][top]

[top]: #markdown-header-layouts
[wrappers]: ../wrappers/