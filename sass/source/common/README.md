# Common

O diretório `common` contém todas as regras, módulos, configurações, etc. que são comuns para todos os tamanhos de tela.

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/common/** - você está aqui;
    * **core/** - toda configuração, funções e mixins. [Mais detalhes][core];
    * **layouts/** - diagramações de páginas e regras específicas por escopo. [Mais detalhes][layouts];
    * **modules/** - módulos utilizados nas diagramações. [Mais detalhes][modules];
    * **wrappers/** - agrupamento de imports. [Mais detalhes][wrappers];

[↩][top]

## Core

Tudo referente à configuração, como definição de grids, cores, unidades de medida, funções, extends e mixins estão dentro do core. A idéia do core é que este seja escrito em todos os arquivos presentes na pasta `output`, portanto, apenas declaram variáveis e definem funções / mixins e ***não escreve nada na saída do CSS***. [Veja mais detalhes](core/).

[↩][top]

## Layouts

Tudo referente à diagramação. A idéia é ter um lay-out por tipo de página do site. Normalmente nesta situação que os módulos ficarão lado-a-lado, por exemplo, ou com algum tamanho fixo, caso necessário. Aqui também entram regras de espaçamento específicas por escopo. [Veja mais detalhes e exemplos de como os layouts funcionam](layouts/).

[↩][top]

## Modules

Conterá a maior parte dos arquivos. De acordo com a análise do projeto, os módulos devem ser separados e cada módulo deve estar em um arquivo scss com suas variações. Quanto mais granulado, menos engessado fica o projeto. Alguns exemplos de módulos:

* galeria de fotos;
* botões;
* cards;
* players / controles;
* tabelas;
* diagramação de texto, como uma descrição de produto com preço;
* estrelas de rankings;
* diagramação de formulários;
* banners;
* lista de produtos;
* menus de navegação, breadcrumbs;
* etc.

Apesar de não existir uma regra absoluta para a quantidade de linhas que um módulo deve ter, no geral, costumam ter no máximo 100 linhas. Mesmo se um módulo complexo tiver mais que isso, provavelmente ele pode ser quebrado em peças menores.

[Veja mais detalhes e exemplos de módulos e como organizá-los internamente](modules/).

[↩][top]

## Wrappers

"Wrappers" são arquivos que não contém definição, apenas importam grupos de arquivos para facilitar a manutenção. Este recurso deve ser usado com muito cuidado para evitar que imports sejam duplicados no arquivo final, em `output`. [Veja mais detalhes e exemplos de "wrappers"](wrappers/).

**IMPORTANTE**    
Os wrappers são os únicos tipos de arquivos que poderão ter import além dos outputs. Módulos, Layouts, Extends e Cores nunca devem conter `@import`.

[top]: #markdown-header-common
[modules]: modules/
[wrappers]: wrappers/
[layouts]: layouts/
[core]: core/