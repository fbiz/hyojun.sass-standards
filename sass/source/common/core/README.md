# Core

Aqui estão todos os arquivos de configuração (variáveis) e lógica (functions, mixins, etc.) do projeto.

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/common/core/** - você está aqui;
    * **animations/** - eventuais animações em css. [Mais detalhes][animations];
    * **config/** - configurações do projeto como variáveis de cores e unidades, famílias tipográficas, grids e mixins. [Mais detalhes][config];
    * **extends/** - elementos a serem extendidos. [Mais detalhes][extends];
    * **functions/** - as funções do projeto. [Mais detalhes][functions];
    * **mixins/** - os mixins do projeto. [Mais detalhes][mixins];

Note que nenhum arquivo deve existir dentro deste diretório.

[↩][top]

[top]: #markdown-header-core
[animations]: animations/
[config]: config/
[extends]: extends/
[functions]: functions/
[mixins]: mixins/