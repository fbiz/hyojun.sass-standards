# Extends

Diretório com todos os [placeholder selectors][] (`%foo`) referentes à base dos módulos que não contém variação.

Muitas vezes a criação de mixins estáticos geram um aumento considerável de código no arquivo final, o uso do extend é útil para evitar que o mesmo código seja re-escrito na página muitas vezes e fica agrupado no topo do arquivo. Veja mais informações sobre extend utilizando [placeholder selectors][] no SASS.

A idéia do extends é que seja escrito em todos os arquivos presentes na pasta `output`, portanto, apenas declaram placeholders, e não escrevem nada na saída do CSS. [Veja como funciona a ordem de imports][wrappers].

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/common/core/extends/** - você está aqui;

[↩][top]

## Entenda quando criar um extend

Existem algumas diferenças básicas entre `@extend` e `@mixin` — veja aqui [quando criar mixins][]. O método de extensão deve ser usado para o que o nome indica: extensão de atributos de um módulo base para módulos compostos. No primeiro momento que o @extend é chamado, todos os próximos serão agrupados naquele ponto do código, ou seja, se não houver controle das regras de hierarquia usadas nos módulos, haverá dificuldade de entender o que está acontecendo no momento de manutenção.

Não existe lógica de programação (condições, argumentos, etc.) para extenção de elementos.

Apenas a extensão de placeholders deve ser usado, alguns exemplos de situações onde o extend é aceito:

* O código base a ser extendido é longo, como a técnica de wrapper para float. Ex.:

        #!sass
        %float-wrapper {
          &:after {
            content: "";
            display: block;
            height: 0;
            overflow: hidden;
            clear: both;
          }
        }

    Como este código não muda, é relativamente grande e será usado por todos os elementos que contiverem algum float dentro, vale a pena usá-lo como `@extend`.

* Tenho um módulo base que extenderia usando múltiplas classes. Ex.: "base-button arrow-button highlight-button". Uma solução para aproveitar o poder do SASS e evitar muitas classes escritas no HTML final seria:

        #!sass
        %base-button { // botão padrão do site
          background: $base-button-bg-color;
          border-radius: $base-button-border-radius;
          font-size: $content-font-size;
          color: $base-button-font-color;
          padding: to-rem(5px);
          display: inline-block;
        }
        // ...
        %arrow-button { // botão com setinha
          @extend %arrow-button;
          position: relative;
          padding-left: w($base-arrow-size) + to-rem(10px);
          &:before {
            content: "";
            position: absolute;
            left: to-rem(5px);
            @include arrow($base-arrow-size);
            @include absolute-center($base-arrow-size, y);
          }
        }
        // ... dentro da pasta modules
        .arrow-button{
          @extend %arrow-button;
        }
        // ...
        .highlight-button { // botão grande e chamativo com setinha
          @extend %arrow-button;
          font-size: $highlight-font-size;
          padding: {
            top: to-rem(10px);
            right: to-rem(10px);
            bottom: to-rem(10px);
          }
          background: $highlight-button-bg-color;
          color: $highlight-button-font-color;
        }

[↩][top]

[top]: #markdown-header-extends
[placeholder selectors]: http://sass-lang.com/documentation/file.SASS_REFERENCE.html#placeholder_selectors_
[quando criar mixins]: ../mixins/
[wrappers]: ../wrappers/