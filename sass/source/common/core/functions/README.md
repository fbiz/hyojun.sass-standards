# Functions

Diretório que recebe todas as funções específicas do projeto. Normalmente as funções são utilizadas para cálculos que retornam algum valor específico.

A idéia é que seja escrito em todos os arquivos presentes na pasta `output`, ou seja, contém apenas declarações e não escrevem nada na saída do CSS. [Veja como funciona a ordem de imports][wrappers].

Conteúdo da página:

[TOC]

## Organização dos diretórios

Este escopo contém a seguinte estrutura:

* **~/sass/source/common/core/functions/** - você está aqui;

Caso o projeto não contenha funções, esta pasta, assim como o _functions.scss, podem ser removidos.

[↩][top]

[top]: #markdown-header-functions
[wrappers]: ../../wrappers/